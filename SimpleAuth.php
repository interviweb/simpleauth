<?php
namespace InterVi\SimpleAuth;

/**
* Easy auth functional and CSRF protection. Need started session, or methods will throw Exception.
*
* @author InterVi
* @license GNU LGPL v3
*/
class SimpleAuth {
    private $salt;
    private $hashalgo;

    /**
    * Constructor.
    *
    * @param string $salt Salt for hashing.
    * @param string $hashalgo Algorithm for hashing (md5, sha256, sha512 or etc).
    */
    public function __construct($salt, $hashalgo) {
        $this->salt = $salt;
        $this->hashalgo = $hashalgo;
    }

    /**
    * Generate new or get old CSRF value. Generated value will saving into session.
    *
    * @return string Random value or read from session store.
    */
    public function get_csrf() {
        if (session_status() != PHP_SESSION_ACTIVE) {
            throw new Exception('Session not started.');
        }
        if (empty($_SESSION['csrf'])) {
            $result = hash('sha256', bin2hex(random_bytes(128)));
            $_SESSION['csrf'] = $result;
            return $result;
        } else {
            return $_SESSION['csrf'];
        }
    }

    /**
    * Checking valid CSRF.
    *
    * @param string $csrf CSRF value.
    * @return bool true if is valid value
    */
    public function is_valid_csrf($csrf) {
        if (session_status() != PHP_SESSION_ACTIVE) {
            throw new Exception('Session not started.');
        }
        if (!isset($_SESSION['csrf'])) return false;
        return strcmp($_SESSION['csrf'], $csrf) === 0;
    }

    /**
    * Check is authed.
    *
    * @return bool if authed
    */
    public function is_authed() {
        if (session_status() != PHP_SESSION_ACTIVE) {
            throw new Exception('Session not started.');
        }
        if (!isset($_SESSION['authed']) || !$_SESSION['authed']) return false;
        return true;
    }

    /**
    * Get hash from raw password.
    *
    * @param string $password Raw password.
    * @return string Hash from password.
    */
    public function get_hash($password) {
        return hash($this->hashalgo, hash($this->hashalgo, $password) . $this->salt);
    }

    /**
    * Attempt auth.
    *
    * @param string $password Raw password.
    * @param string $hash Hash from password (need for check raw password).
    * @return bool true if success auth
    */
    public function auth($password, $hash) {
        if (session_status() != PHP_SESSION_ACTIVE) {
            throw new Exception('Session not started.');
        }
        if (strcmp($this->get_hash($password), $hash) !== 0) return false;
        $_SESSION['authed'] = true;
        return true;
    }

    /**
    * Logout.
    */
    public function logout() {
        if (session_status() != PHP_SESSION_ACTIVE) {
            throw new Exception('Session not started.');
        }
        $_SESSION['authed'] = false;
    }
}
