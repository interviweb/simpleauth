# SimpleAuth

Providing easy auth system and CSRF protection functional. See API.

## API

Need started session, or methods will throw Exception.

* **__construct($salt, $hashalgo)** - Salt is you random string, hashalgo is hash algorithm (see [php docs](http://php.net/manual/ru/function.hash.php))
* **get_csrf()** - return string, generate new or get old CSRF value. Generated value will saving into session.
* **is_valid_csrf($csrf)** - return bool, checking valid CSRF
* **is_authed()** - return bool, check is authed
* **get_hash($password)** - return string, hashed password. $password is raw string password.
* **auth($password, $hash)** - return bool, attempt auth. $password is raw password (string), $hash is hashed password (string).
* **logout()** - logout

## Example

```
use InterVi\SimpleAuth\SimpleAuth;

session_start();
$user = new User(); //you custom user

$auth = new SimpleAuth('JKhggghgFdfs33ds', 'sha256');
if ($auth->is_authed()) {
    //example CSRF checking
    $csrf = filter_input(INPUT_POST, 'csrf');
    if (!$auth->is_valid_csrf($csrf)) {
        http_response_code(401);
        die('401 Unauthorized');
    }
} else {
    //example auth
    $data = json_decode(file_get_contents('php://input'), true);
    if (!$auth->auth($data['password'])) {
        http_response_code(401);
        die('401 Unauthorized');
    }
}
```
